<?php

namespace Drupal\menu_link_absolute_to_internal\EventSubscriber;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\core_event_dispatcher\Event\Form\FormAlterEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Url;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Subscribe to form events (you can alter forms here) and alter them.
 *
 * SEE: hook_event_dispatcher/examples/ExampleFormEventSubscribers.php .
 */
class MenuLinkAbsoluteToInternalFormEventListener implements EventSubscriberInterface {

  /**
   * Messenger service.
   *
   * @var Drupal\Core\Logger\LoggerChannel
   */
  protected $logger;

  /**
   * The configuration relating to this module.
   *
   * @var Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Constructor.
   */
  public function __construct(
    LoggerChannelFactory $loggerFactory,
    ConfigFactoryInterface $configFactory
  ) {
    $this->logger = $loggerFactory->get('ohsuwww_profiles');
    $this->config = $configFactory->get('menu_link_absolute_to_internal.settings');
  }

  /**
   * {@inheritdoc}
   *
   * Establish what events this subscriber responds to.
   */
  public static function getSubscribedEvents(): array {
    return [
      HookEventDispatcherInterface::FORM_ALTER => 'alterForm',
    ];
  }

  /**
   * EQUIVALENT: hook_form_alter.
   *
   * @param \Drupal\core_event_dispatcher\Event\Form\FormAlterEvent $event
   *   The event.
   */
  public function alterForm(FormAlterEvent $event): void {
    $form_id = $event->getFormId();

    if (
      $this->config->get('menu_mode') !== 'disabled'
      && $form_id == 'menu_link_content_menu_link_content_form'
    ) {
      $form = &$event->getForm();
      $form['#validate'][] = get_class($this) . '::linkNotDirectWhenLocal';
      return;
    }
  }

  /**
   * Checks that a link submission is not direct to content when local.
   *
   * IE: Do not use https://www.example.com/about
   * Instead use the internal path such as internal:/node/1234
   *
   * This will allow the link to always track to the correct place, even if the
   * entity gets renamed.
   */
  public static function linkNotDirectWhenLocal(&$form, FormStateInterface $form_state) {
    if ($form_state->hasValue('link')) {

      $link_uri = $form_state->getValue('link')[0]['uri'];
      $config = \Drupal::config('menu_link_absolute_to_internal.settings');

      if ($config->get('menu_mode') === 'auto') {
        $baseUrl = \Drupal::request()->getSchemeAndHttpHost();

        // Exit early if we don't have a string match!
        if (strpos($link_uri, $baseUrl) === FALSE) {
          return;
        }
        $possibly_local_path = str_replace(
          $baseUrl,
          '',
          $link_uri
        );
      }
      else {
        $url_match = FALSE;
        $url_info = parse_url($link_uri);
        // Get and convert base URLs.
        $baseUrls = $config->get('base_urls');
        $baseUrls = preg_replace("/\s+/", "\n", $baseUrls);
        $baseUrls = str_replace(",", "\n", $baseUrls);
        $base_url_array = explode("\n", $baseUrls);
        $base_url_array = array_filter($base_url_array);
        foreach ($base_url_array as $base_url) {
          $base_url_array_trim[] = parse_url($base_url)['host'];
        }
        if (isset($url_info['host'])) {
          $url_match = array_search($url_info['host'], $base_url_array_trim);
        }

        // Exit early if we don't have a string match!
        if ($url_match === FALSE) {
          return;
        }
        $possibly_local_path = $url_info['path'];
      }

      // Use URL to see if we've got a routed path for this alias.
      $local_path = url::fromUserInput($possibly_local_path);

      // If the URl is internal and routed, adjust link to internal content!
      if (!$local_path->isExternal() && $local_path->isRouted()) {
        $link_values = $form_state->getValue('link');

        // * NOTE: Route handling beyond node will be made available in:
        // * https://www.drupal.org/node/2423093.
        switch ($local_path->getRouteName()) {
          // Entity link handling.
          case "entity.node.canonical":
            $link_values[0]['uri'] = 'entity:' . $local_path->getInternalPath();
            break;

          // Internal link handling by default if no other known handling.
          default:
            $link_values[0]['uri'] = 'internal:/' . $local_path->getInternalPath();
            break;
        }

        $form_state->setValue('link', $link_values);
      }
    }
  }

}
