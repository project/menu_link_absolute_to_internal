<?php

namespace Drupal\menu_link_absolute_to_internal\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;

/**
 * Administration Form for Menu_link_absolute_to_internal.
 */
class MenuLinkAbsoluteToInternalConfigForm extends ConfigFormBase {

  /**
   * The configuration name to be used throughout this form.
   *
   * @var string
   */
  private string $configName = 'menu_link_absolute_to_internal.settings';

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      $this->configName,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'menu_link_absolute_to_internal_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config($this->configName);

    $menu_mode = $config->get('menu_mode') ? $config->get('menu_mode') : 'auto';

    $form['menu_mode_selection'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Menu Mode'),
      '#description' => $this->t('Select what mode you want to have Menu Link Absolute to Internal adjusting links with. Automatic will determine the URL of the site, but may not be ideal when utilizing local or development/staging environments and/or writing tests.'),
      '#required' => TRUE,
      '#options' => [
        'disabled' => $this->t('Disabled'),
        'auto' => $this->t('Determine Base Url automagically'),
        'manual' => $this->t('Use provided base url(s)'),
      ],
      '#default_value' => $menu_mode,
    ];

    $form['base_urls'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Address'),
      '#description' => $this->t('Enter all of the base URLs you want to look for internal URLs for. Enter one URL per line.'),
      '#rows' => 4,
      '#cols' => 5,
      '#states' => [
        'visible' => [
          ':input[name="menu_mode_selection"]' => ['value' => 'manual'],
        ],
      ],
      '#default_value' => $config->get('base_urls'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $baseUrls = $form_state->getValue('base_urls');
    $baseUrls = preg_replace("/\s+/", "\n", $baseUrls);
    $baseUrls = str_replace(",", "\n", $baseUrls);
    $base_url_array = explode("\n", $baseUrls);
    $base_url_array = array_filter($base_url_array);
    foreach ($base_url_array as $url) {
      if ((strpos($url, 'http') === FALSE) && (strpos($url, 'https') === FALSE)) {
        $form_state->setError($form['base_urls'], 'Scheme unsupported!');
      }
      if (!UrlHelper::isValid($url, TRUE)) {
        $form_state->setError($form['base_urls'], 'The URL is invalid.');
      }
      if (strpos($url, '*') !== FALSE) {
        $form_state->setError($form['base_urls'], 'URL invalidations should not contain asterisks!');
      }
      if (strpos($url, ' ') !== FALSE) {
        $form_state->setError($form['base_urls'], 'URL invalidations cannot contain spaces, use %20 instead.');
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config($this->configName)
      ->set('menu_mode', $form_state->getValue('menu_mode_selection'))
      ->set('base_urls', $form_state->getValue('base_urls'))
      ->save();
  }

}
